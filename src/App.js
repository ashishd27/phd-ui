import React,{useState} from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Dashboard from './dashboard'

import Login from "./Login";
import SignUp from "./Signup";
import {Wrapper} from './Wrapper'
import Videos from './Videos'
import {Analysis} from './Analysis'

export const DataContext = React.createContext('light');


function App() {
  const [isAuthorized, setIsAuthorized] =useState(false)

  return (<DataContext.Provider value ={{data:{isAuthorized}}}><Router>
    <div className="App">
    <div className="auth-wrapper" style={{overflow:"scroll" ,marginTop:"50px",width:"100%", height:"100%"}}>
          <Switch>
            <Route exact path='/' render={()=><Login setIsAuthorized={setIsAuthorized}/>} />
            <Route path="/sign-in" render={()=><Login setIsAuthorized={setIsAuthorized}/>} />
            <Route path="/sign-up" render={()=><SignUp setIsAuthorized={setIsAuthorized}/>} />
            <Route path="/Dashboard"  render={()=><Dashboard isAuthorized={isAuthorized}/>} />
            <Route path="/MCQ/:id" exact render={()=><Wrapper isAuthorized={isAuthorized}/> }/>
            <Route path="/video" exact render={()=><Videos isAuthorized={isAuthorized}/> }/>
            <Route path="/reports" exact render={()=><Analysis isAuthorized={isAuthorized}/> }/>

          </Switch>
        </div>
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/Dashboard"}><img src="./1.png" height={80} width="80"/></Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
            </ul>
          </div>
        {isAuthorized &&  <button className="btn btn-primary btn-lg" onClick={()=>{
            sessionStorage.clear();
          setIsAuthorized(false)
         }}>Logout</button>}
        </div>
      </nav>

     
    </div></Router></DataContext.Provider>
  );
}

export default App;